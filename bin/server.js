// @ts-check
import express from 'express';
// @ts-ignore - File is generated during build
import { main } from './dist/main.js'

const server = express();

server.use('/api', main);
server.use(express.static('public'));

server.listen(3000);
