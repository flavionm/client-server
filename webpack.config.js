// @ts-check
/** @type {import('webpack').Configuration} */
export default {
	entry: './client/index.ts',
	module: {
		rules: [
			{
				test: /\.ts$/,
				use: [
					{
						loader: 'ts-loader',
						options: {
							configFile: 'client/tsconfig.json'
						}
					}
				],
				exclude: /node-modules/,
			}
		]
	},
	output: {
		filename: 'index.js',
		path: new URL('./public/dist/', import.meta.url).pathname,
		clean: true
	},
	mode: 'production'
};
