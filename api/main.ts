import express from 'express';
import { root } from './root.js';

const main = express.Router();

main.get('/', root);

export { main };
