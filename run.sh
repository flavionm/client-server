#!/bin/sh

IMAGE_NAME=client-server-image

podman build -f Containerfile.dev -t $IMAGE_NAME .
podman run -v .:/app -p 3000:3000 -it --rm $IMAGE_NAME sh
